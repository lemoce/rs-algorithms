
use std::borrow::Cow;

pub struct QuickFindUF<'a> {
    pub id: Cow<'a, Vec<i32>>,
}

impl<'a> QuickFindUF<'a> {
    pub fn new(n: i32) -> QuickFindUF<'a> {
        let vec = (0 .. n).collect::<Vec<_>>();
        QuickFindUF { id: Cow::Owned(vec) }
    }

    pub fn connected(&self, (p, q): (usize, usize)) -> bool {
        self.id[p] == self.id[q]
    }

    pub fn union(&mut self, (p, q): (usize, usize)) {
        let vec = self.id.to_mut();
        let pid = vec[p];
        let qid = vec[q];

        for id in vec.iter_mut() {
            if *id == pid {
                *id = qid;
            }
        }
    }
}
