use std::borrow::Cow;

pub struct QuickUnionUF<'a> {
    pub id: Cow<'a, Vec<i32>>,
}

impl<'a> QuickUnionUF<'a> {
    pub fn new(n: i32) -> QuickUnionUF<'a> {
        let vec = (0 .. n).collect::<Vec<_>>();
        QuickUnionUF { id: Cow::Owned(vec) }
    }

    fn root(&self, i: i32) -> i32 {
        let mut j = i;
        while j != self.id[j as usize] {
            j = self.id[j as usize];
        }
        j
    }
    
    pub fn connected(&self, (p, q): (usize, usize)) -> bool {
        self.root(p as i32) == self.root(q as i32)
    }

    pub fn union(&mut self, (p, q): (usize, usize)) {
        let i = self.root(p as i32);
        let j = self.root(q as i32);

        let vec = self.id.to_mut();
        vec[i as usize] = j as i32;
    }
}

pub struct QuickPCUnionUF<'a> {
    pub id: Cow<'a, Vec<i32>>,
}

impl<'a> QuickPCUnionUF<'a> {
    pub fn new(n: i32) -> QuickPCUnionUF<'a> {
        let vec = (0 .. n).collect::<Vec<_>>();
        QuickPCUnionUF { id: Cow::Owned(vec) }
    }

    fn root(&mut self, i: i32) -> i32 {
        let mut j = i;
        let ids = self.id.to_mut();
        while j != ids[j as usize] {
            ids[j as usize] = ids[ids[j as usize] as usize];
            j = ids[j as usize];
        }
        j
    }
    
    pub fn connected(&mut self, (p, q): (usize, usize)) -> bool {
        self.root(p as i32) == self.root(q as i32)
    }

    pub fn union(&mut self, (p, q): (usize, usize)) {
        let i = self.root(p as i32);
        let j = self.root(q as i32);

        let vec = self.id.to_mut();
        vec[i as usize] = j as i32;
    }
}

pub struct QuickWUnionUF<'a> {
    pub id: Cow<'a, Vec<i32>>,
    pub sz: Cow<'a, Vec<i32>>,
}

impl<'a> QuickWUnionUF<'a> {
    pub fn new(n: i32) -> QuickWUnionUF<'a> {
        let vec = (0 .. n).collect::<Vec<_>>();
        let svec = vec![0_i32; n as usize];
        QuickWUnionUF { id: Cow::Owned(vec), sz: Cow::Owned(svec) }
    }

    fn root(&self, i: i32) -> i32 {
        let mut j = i;
        while j != self.id[j as usize] {
            j = self.id[j as usize];
        }
        j
    }
    
    pub fn connected(&self, (p, q): (usize, usize)) -> bool {
        self.root(p as i32) == self.root(q as i32)
    }

    pub fn union(&mut self, (p, q): (usize, usize)) {
        let i = self.root(p as i32);
        let j = self.root(q as i32);

        let ids = self.id.to_mut();
        let heights = self.sz.to_mut();

        if heights[i as usize] < heights[j as usize] {
            ids[i as usize] = j;
            heights[j as usize] = heights[i as usize] + 1;
        } else {
            ids[j as usize] = i;
            heights[i as usize] += heights[j as usize] + 1;
        }
    }
}
