
use std::io::BufRead;

use algorithms::quickfind::QuickFindUF;

macro_rules! scan {
    ( $string:expr, $sep:expr, $( $x:ty ),+ ) => {{
        let mut iter = $string.split($sep);
        ($(iter.next().and_then(|word| word.parse::<$x>().ok()),)*)
    }}
}


fn main() {
    let mut buf = String::new();
    let stdin = std::io::stdin();
    let mut handle = stdin.lock();
    handle.read_line(&mut buf).unwrap();
    
    let n = scan!(buf, char::is_whitespace, i32);
    let mut uf = QuickFindUF::new(n.0.unwrap());

    for line in handle.lines() {
        let l = line.unwrap();
        let pair = scan!(l, char::is_whitespace, usize, usize);
        let p = pair.0.unwrap();
        let q = pair.1.unwrap();
        if !uf.connected((p, q)) {
            uf.union((p, q));
            println!("{} - {}", p, q);
        }
    }

    println!("{:?}", uf.id);
}
